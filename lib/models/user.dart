class User {
  final String email;
  final String name;
  final String avatarUrl;

  User({
    required this.email,
    required this.name,
    required this.avatarUrl,
  });

  factory User.fromJson(Map<String, dynamic> map) {
    return User(
      email: map['email'] as String,
      name: map['name'] as String,
      avatarUrl: map['picture']['data']['url'] as String,
    );
  }

  @override
  String toString() {
    return "$email $name $avatarUrl";
  }
}
