import 'dart:async';

import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:logger/logger.dart';

class AuthRepository {
  FutureOr<void> loginWithFacebook() async {
    // by default we request the email and the public profile
    final LoginResult result = await FacebookAuth.instance.login();

    if (result.status == LoginStatus.success) {
      // you are logged
      final AccessToken accessToken = result.accessToken!;
      Logger().i(
        "${result.status} | ${result.message} | AccessToken: ${result.accessToken!.token}",
      );
    } else {
      Logger().i(
        "${result.status} | ${result.message}",
      );
    }
  }

  FutureOr<bool> isLoggedIn() async {
    final accessToken = await FacebookAuth.instance.accessToken;

    if (accessToken != null) {
      return true;
    } else {
      return false;
    }
  }

  FutureOr<void> logOut() async {
    await FacebookAuth.instance.logOut();
  }
}
