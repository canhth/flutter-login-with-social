import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:flutter_login_with_social/models/user.dart';
import 'package:flutter_login_with_social/repository/auth_repository.dart';
import 'package:logger/logger.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  User? user;

  @override
  void initState() {
    super.initState();
    _getUserData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          if (user != null)
            Column(
              children: [
                Text(user!.name),
                Text(user!.email),
                Image.network(user!.avatarUrl),
              ],
            ),
          FilledButton(
            onPressed: () async {
              await AuthRepository().logOut();
              if (context.mounted) Navigator.pop(context);
            },
            child: const Text('Logout'),
          )
        ],
      ),
    );
  }

  FutureOr<User?> _getUserData() async {
    final userData = await FacebookAuth.instance.getUserData();
    setState(() {
      user = User.fromJson(userData);
    });
    Logger().i(user?.toString());
    return user;
  }
}
