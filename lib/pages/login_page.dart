import 'package:flutter/material.dart';
import 'package:flutter_login_with_social/repository/auth_repository.dart';

import 'home_page.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FilledButton(
              onPressed: () {
                _loginWithFacebook(
                  () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => const HomePage(),
                    ));
                  },
                );
              },
              child: const Text('Login with Facebook'),
            )
          ],
        ),
      ),
    );
  }

  void _loginWithFacebook(VoidCallback callback) async {
    await AuthRepository().loginWithFacebook();

    callback.call();
  }
}
